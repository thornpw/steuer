# Version 0.2.2 - 10.07.2016
Changes to better work together with a pygame kernel
No logging configuration file is necessary anymore

# Version 0.2.1 - 31.01.2016
Bugs fixed in the examples.

# Version 0.2 - 31.01.2016
debugged deployment

# Version 0.1 - 27.01.2016
initial deployment
